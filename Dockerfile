FROM jboss-eap64-openshift:latest
#USER root

ADD java/monitorFlussiWebColl.war /opt/eap/standalone/webapps/
ADD standalone.xml /opt/eap/standalone/configuration/
RUN wget http://search.maven.org/remotecontent?filepath=org/postgresql/postgresql/9.3-1102-jdbc41/postgresql-9.3-1102-jdbc41.jar
    mv postgresql-9.3-1102-jdbc41.jar /opt/eap/standalone/webapps
    #ifconfig eth0 hw ether 02:42:0a:01:01:22
    
EXPOSE 8080 9990 22
CMD ["/opt/eap/bin/standalone.sh","-b","0.0.0.0","-c","/opt/eap/standalone/configuration/standalone.xml"]
